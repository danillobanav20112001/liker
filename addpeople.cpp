#include "firstproject.h"
#include "addpeople.h"
#include "ui_addpeople.h"
#include <QWidget>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include "QSqlDatabase"
#include "QSqlQuery"
#include <QSqlQueryModel>
#include <QSqlTableModel>

addPeople::addPeople(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addPeople)
{

    ui->setupUi(this);
    ui->lineEdit_4->setText("0");
    QSqlDatabase famaly=QSqlDatabase::addDatabase("QSQLITE");
    famaly.setDatabaseName("dbfamily.sqlite");

    if (!famaly.open())
        {
            qDebug() << famaly.lastError().text();
            ui->status->setText("Подключение не удалось");
            return;
        }else{
        ui->status->setText("Подключился");
             }
}

addPeople::~addPeople()
{
    delete ui;
}

void addPeople::on_pushButton_clicked()
{


        QSqlQuery query;

    query.prepare("INSERT INTO  family (Имя, Фамилия, Отчество, Пол,Капитал) VALUES(?, ?, ?, ?,?)"); // подготавливаем запрос

    query.addBindValue(ui->lineEdit->text());
    query.addBindValue(ui->lineEdit_2->text());
    query.addBindValue(ui->lineEdit_3->text());
    query.addBindValue(ui->comboBox->currentText());
    query.addBindValue(ui->lineEdit_4->text());
    if (!query.exec()) { // выполняем готовый запрос
      qDebug()<<"Error";
    }
  QString base=ui->lineEdit->text();
  QString base1=ui->lineEdit_2->text();

        query.prepare( "CREATE TABLE IF NOT EXISTS " + base +"_"+ base1 + " (id INTEGER UNIQUE PRIMARY KEY, Дата VARCHAR(30), Категория VARCHAR(30), Товар VARCHAR(30), Сумма VARCHAR(30) , Капитал VARCHAR(100), Комментарий VARCHAR(100))" );
        if(query.exec())
        {

        }
        else
        {
        }
    close();
}
