#ifndef FORMAPEOPLE_H
#define FORMAPEOPLE_H

#include <QDialog>

namespace Ui {
class FormaPeople;
}

class FormaPeople : public QDialog
{
    Q_OBJECT

public:
    explicit FormaPeople(QWidget *parent = nullptr);
    ~FormaPeople();

private slots:
    void on_newPeople_clicked();

private:
    Ui::FormaPeople *ui;
};

#endif // FORMAPEOPLE_H
