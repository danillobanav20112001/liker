#ifndef HISTORYFAMAL_H
#define HISTORYFAMAL_H

#include <QDialog>

namespace Ui {
class historyfamal;
}

class historyfamal : public QDialog
{
    Q_OBJECT

public:
    explicit historyfamal(QWidget *parent = nullptr);
    ~historyfamal();

private slots:
    void on_pushButton_clicked();

private:
    Ui::historyfamal *ui;
};

#endif // HISTORYFAMAL_H
