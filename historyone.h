#ifndef HISTORYONE_H
#define HISTORYONE_H
#include "historyfamal.h"
#include <QDialog>

namespace Ui {
class historyOne;
}

class historyOne : public QDialog
{
    Q_OBJECT

public:
    explicit historyOne(QWidget *parent = nullptr);
    ~historyOne();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::historyOne *ui;
    historyfamal *window9;
};

#endif // HISTORYONE_H
