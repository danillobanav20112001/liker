#include "firstproject.h"
#include "addpeople.h"
#include "formapeople.h"
#include "generalwin.h"
#include "historyfamal.h"
#include "historyone.h"
#include "removpeople.h"
#include "sendfamal.h"
#include "sendmoney.h"
#include "statistic.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FirstProject w;
    w.show();
    return a.exec();
}
