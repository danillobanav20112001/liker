#ifndef NOMONEY_H
#define NOMONEY_H
#include "nomoney2.h"
#include <QDialog>

namespace Ui {
class Nomoney;
}

class Nomoney : public QDialog
{
    Q_OBJECT

public:
    explicit Nomoney(QWidget *parent = nullptr);
    ~Nomoney();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Nomoney *ui;
    Nomoney2 *window11;
};

#endif // NOMONEY_H
