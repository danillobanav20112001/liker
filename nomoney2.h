#ifndef NOMONEY2_H
#define NOMONEY2_H

#include <QDialog>

namespace Ui {
class Nomoney2;
}

class Nomoney2 : public QDialog
{
    Q_OBJECT

public:
    explicit Nomoney2(QWidget *parent = nullptr);
    ~Nomoney2();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Nomoney2 *ui;
};

#endif // NOMONEY2_H
