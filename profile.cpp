#include "profile.h"
#include "ui_profile.h"
extern QString name;
extern QString surname;
extern QString middlename;
extern QString Woman;
Profile::Profile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Profile)
{
    ui->setupUi(this);
    ui->lineEdit->setText(name);
    ui->lineEdit_2->setText(surname);
    ui->lineEdit_3->setText(middlename);
    if(Woman=="Ж"){
        ui->comboBox->setCurrentIndex(1);
    }else{
        ui->comboBox->setCurrentIndex(0);
    }
}

Profile::~Profile()
{
    delete ui;
}

void Profile::on_pushButton_2_clicked()
{
    close();
}
