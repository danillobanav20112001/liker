#ifndef SENDFAMAL_H
#define SENDFAMAL_H
#include "formapeople.h"
#include <QDialog>
#include <QWidget>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include "QSqlDatabase"
#include "QSqlQuery"

namespace Ui {
class SendFamal;
}

class SendFamal : public QDialog
{
    Q_OBJECT

public:
    explicit SendFamal(QWidget *parent = nullptr);
    ~SendFamal();

private slots:
    void on_pushButton_clicked();

private:
    Ui::SendFamal *ui;
    FormaPeople *window2;
    QSqlDatabase dbfamal;
};

#endif // SENDFAMAL_H
