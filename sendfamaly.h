#ifndef SENDFAMALY_H
#define SENDFAMALY_H

#include <QWidget>

namespace Ui {
class SendFamaly;
}

class SendFamaly : public QWidget
{
    Q_OBJECT

public:
    explicit SendFamaly(QWidget *parent = nullptr);
    ~SendFamaly();

private:
    Ui::SendFamaly *ui;
};

#endif // SENDFAMALY_H
