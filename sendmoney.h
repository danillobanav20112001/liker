#ifndef SENDMONEY_H
#define SENDMONEY_H

#include <QDialog>
#include <QDialog>
#include <QSqlTableModel>
namespace Ui {
class sendmoney;
}

class sendmoney : public QDialog
{
    Q_OBJECT

public:
    explicit sendmoney(QWidget *parent = nullptr);
    ~sendmoney();

private slots:
    void on_pushButton_clicked();

private:
    Ui::sendmoney *ui;
    QSqlTableModel *model4;
};

#endif // SENDMONEY_H
