#include "testwin.h"
#include "ui_testwin.h"

Testwin::Testwin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Testwin)
{
    ui->setupUi(this);
    qwerty=new QDataWidgetMapper(this);

}

Testwin::~Testwin()
{
    delete ui;
}

void Testwin::setModel(QAbstractItemModel *mops)
{qwerty->setModel(mops);
    qwerty->addMapping(ui->comboBox, 1);
    qwerty->addMapping(ui->spinBox, 2);
    qwerty->addMapping(ui->lineEdit, 3);
}

void Testwin::on_pushButton_clicked()
{
    qwerty->submit();
}

void Testwin::on_pushButton_2_clicked()
{
    close();
}
